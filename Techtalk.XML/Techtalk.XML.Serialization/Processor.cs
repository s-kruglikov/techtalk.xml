﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Techtalk.XML.Resources;

namespace Techtalk.XML.Serialization
{
	public class Processor
	{
		#region Private Fields

		private readonly Helper _helper;

		#endregion

		#region Constructor

		public Processor()
		{
			_helper = new Helper();
		}

		#endregion

		#region Public Interface

		/// <summary>
		/// Serializes Orders objects collection into Xml.
		/// </summary>
		/// <returns>String representation of the serialized Xml.</returns>
		public string SerializeDefaultOrders()
		{
			var orders = GetOrders();

			using (var resultStream = new MemoryStream())
			{
				var serializer = new XmlSerializer(typeof(OrdersCollection));

				serializer.Serialize(resultStream, orders);

				//Show results
				resultStream.Seek(0, SeekOrigin.Begin);
				using (var reader = new StreamReader(resultStream))
				{
					return reader.ReadToEnd();
				}
			}
		}

		/// <summary>
		/// Serializes Orders objects collection into Xml with specific xml structure.
		/// </summary>
		/// <returns>String representation of the serialized Xml.</returns>
		public string SerializeOrders()
		{
			var orders = GetOrders();

			using (var resultStream = new MemoryStream())
			{
				var serializerNamespaces = new XmlSerializerNamespaces();
				serializerNamespaces.Add(string.Empty, Constants.OrdersNamespace);
				serializerNamespaces.Add("ns1", Constants.PricesNamespace);

				var serializer = new XmlSerializer(typeof(OrdersCollection));

				var writerSettings = new XmlWriterSettings() { Indent = true, OmitXmlDeclaration = true };
				var resultWriter = XmlWriter.Create(resultStream, writerSettings);

				serializer.Serialize(resultWriter, orders, serializerNamespaces);

				//Show results
				resultStream.Seek(0, SeekOrigin.Begin);
				using (var reader = new StreamReader(resultStream))
				{
					return reader.ReadToEnd();
				}
			}
		}

		/// <summary>
		/// Deserializes Xml into OrdersCollection.
		/// </summary>
		/// <returns>Orders collection.</returns>
		public string DeserializeOrders()
		{
			var ordersStream = _helper.GetOrdersStream();

			var serializer = new XmlSerializer(typeof(OrdersCollection));

			var orders = (OrdersCollection)serializer.Deserialize(ordersStream);

			var resultBuilder = new StringBuilder();
			foreach (var order in orders)
			{
				resultBuilder.Append(order);
			}

			return resultBuilder.ToString();
		}

		#endregion

		#region Internal Implementations

		/// <summary>
		/// Gets orders collection.
		/// </summary>
		/// <returns>Mock orders collection.</returns>
		private OrdersCollection GetOrders()
		{
			return new OrdersCollection
			{
				new Order
				{
					Id = 1,
					Item = "Duff Beer",
					CustomerName = "Homer Simpson",
					Count = 4,
					Price = new Price
					{
						TotalPrice = 12.50M,
						Discount = true
					},
				},
				new Order
				{
					Id = 2,
					Item = "Cherry Pie",
					CustomerName = "Marge Simpson",
					Count = 1,
					Price = new Price
					{
						TotalPrice = 5.15M,
						Discount = false
					}
				}
			};
		}

		#endregion
	}
}
