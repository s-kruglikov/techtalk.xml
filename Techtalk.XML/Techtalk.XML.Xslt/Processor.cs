﻿using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Techtalk.XML.Resources;

namespace Techtalk.XML.Xslt
{
	public class Processor
	{
		#region Private Fields

		private readonly Helper _helper;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes new instance of the Xslt.Processor class.
		/// </summary>
		public Processor()
		{
			_helper = new Helper();
		}

		#endregion

		#region Public Interface

		/// <summary>
		/// Applies transformation on Orders xml data.
		/// </summary>
		/// <returns>Taxes data calculated inside stylesheet.</returns>
		public string ApplyTransformation()
		{
			var ordersStream = _helper.GetOrdersStream();
			var transformationStream = _helper.GetOrdersTransformStream();
			var resultStream = new MemoryStream();

			var ordersXPathDocument = new XPathDocument(ordersStream); // XmlReader can be used
			var transformationXPathDocument = new XPathDocument(transformationStream);

			var writer = XmlWriter.Create(resultStream);

			var transform = new XslCompiledTransform();

			//Enables scripts to be executed in the stylesheet
			var settings = new XsltSettings { EnableScript = true };

			transform.Load(transformationXPathDocument, settings, null);

			// Execute the transformation.
			transform.Transform(ordersXPathDocument, writer);

			//Return result
			resultStream.Seek(0, SeekOrigin.Begin);
			using (var reader = new StreamReader(resultStream))
			{
				return reader.ReadToEnd();
			}
		}

		/// <summary>
		/// Sends parameter and applies transformation on Order xml data.
		/// </summary>
		/// <returns>Taxes data calculeted inside stylesheet.</returns>
		public string ApplyParametrizedTransformation()
		{
			var ordersStream = _helper.GetOrdersStream();
			var transformationStream = _helper.GetOrdersParamTransformStream();
			var resultStream = new MemoryStream();

			var ordersXPathDocument = new XPathDocument(ordersStream); // XmlReader can be used
			var transformationXPathDocument = new XPathDocument(transformationStream);

			var writer = XmlWriter.Create(resultStream);

			// Create the XslCompiledTransform and load the style sheet.
			var transform = new XslCompiledTransform();
			transform.Load(transformationXPathDocument);

			// Create the XsltArgumentList.
			var argList = new XsltArgumentList();
			argList.AddParam("taxPercent", "", "0.5");

			// Execute the transformation.
			transform.Transform(ordersXPathDocument, argList, writer);

			//Return result
			resultStream.Seek(0, SeekOrigin.Begin);
			using (var reader = new StreamReader(resultStream))
			{
				return reader.ReadToEnd();
			}
		}

		#endregion
	}
}
