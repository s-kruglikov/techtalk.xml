﻿using System;

namespace Techtalk.XML.Demo
{
	internal class Program
	{
		public static void Main()
		{
			//var processor = new Techtalk.XML.Reader.Processor();

			//Console.WriteLine(processor.ReadReaderContent());
			//Console.WriteLine(processor.ReadExactNodes());
			//Console.WriteLine(processor.ReadInvalidXml());
			//Console.WriteLine(processor.ReadWithXmlNameTable());

			//var processor = new Writer.Processor();

			//Console.WriteLine(processor.WriteDefaultWriter());
			//Console.WriteLine(processor.WriteConfiguredWriter());
			//Console.WriteLine(processor.AppendByWriter());
			//Console.WriteLine(processor.UpdateByWriter());

			//var processor = new XmlDoc.Processor();
			//Console.WriteLine(processor.ReadDocumentContent());
			//Console.WriteLine(processor.CreateDocumentContent());
			//Console.WriteLine(processor.UpdateDocumentContent());
			//Console.WriteLine(processor.XpathUpdateDocumentContent());

			//var processor = new XDoc.Processor();
			//Console.WriteLine(processor.CreateDocumentContent());
			//Console.WriteLine(processor.ReadDocumentContent());
			//Console.WriteLine(processor.DocumentToObjects());
			//Console.WriteLine(processor.AddDocumentContent());
			//Console.WriteLine(processor.UpdateDocumentContent());

			//var processor = new Serialization.Processor();
			//Console.WriteLine(processor.SerializeDefaultOrders());
			//Console.WriteLine(processor.SerializeOrders());
			//Console.WriteLine(processor.DeserializeOrders());

			//var processor = new Xslt.Processor();
			//Console.WriteLine(processor.ApplyTransformation());
			//Console.WriteLine(processor.ApplyParametrizedTransformation());

			//var processor = new Xsd.Processor();
			//Console.WriteLine(processor.LoadValidateSchema());
			//Console.WriteLine(processor.CreateOrdersSchema());
			//Console.WriteLine(processor.TraverseSchema());
			//Console.WriteLine(processor.UpdateSchema());

			//var processor = new XPathDoc.Processor();
			//Console.WriteLine(processor.ReadDocumentContent());
			//Console.WriteLine(processor.EvaluateXPathExpression());
			//Console.WriteLine(processor.EvaluateWithNamespaces());
			//Console.WriteLine(processor.InsertNodes());
			//Console.WriteLine(processor.EditNodes());

			Console.ReadLine();
		}
	}
}