﻿using System.IO;
using System.Xml;
using System.Xml.Schema;
using Techtalk.XML.Resources;

namespace Techtalk.XML.Writer
{
	/// <summary>
	/// Contains sample methods to demostrate work wih XmlWriter class
	/// </summary>
	public class Processor
	{
		#region Constants

		private const string IndentCharacters = "\t";
		private const string NewLineCharacters = "\r\n";

		#endregion

		#region Public Interface

		/// <summary>
		/// Writes sample content with default XmlWriter
		/// </summary>
		/// <returns>String representation of the result stream.</returns>
		public string WriteDefaultWriter()
		{
			//Use MemoryStream only for demo purpose
			using (var memoryStream = new MemoryStream())
			{
				var writer = XmlWriter.Create(memoryStream);
				
				FillData(writer);

				memoryStream.Seek(0, SeekOrigin.Begin);

				using (var streamReader = new StreamReader(memoryStream))
				{
					return streamReader.ReadToEnd();
				}
			}
		}

		/// <summary>
		/// Writes content using configured writer.
		/// </summary>
		/// <returns>String representation of the result stream.</returns>
		public string WriteConfiguredWriter()
		{
			var writerSettings = new XmlWriterSettings
			{
				ConformanceLevel = ConformanceLevel.Document,
				Indent = true,
				IndentChars = IndentCharacters,
				NewLineChars = NewLineCharacters,
				OmitXmlDeclaration = true
			};

			//Use MemoryStream only for demo purpose
			using (var memoryStream = new MemoryStream())
			{
				var writer = XmlWriter.Create(memoryStream, writerSettings);

				FillData(writer);

				memoryStream.Seek(0, SeekOrigin.Begin);

				using (var streamReader = new StreamReader(memoryStream))
				{
					return streamReader.ReadToEnd();
				}
			}
		}

		/// <summary>
		/// Adds xml log to the end of the file.
		/// </summary>
		/// <returns>String representation of the log file content.</returns>
		public string AppendByWriter()
		{
			using (var streamWriter = File.AppendText(Constants.LogFilePath))
			{
				var xmlTextWriter = new XmlTextWriter(streamWriter)
				{
					Formatting = Formatting.Indented
				};

				xmlTextWriter.WriteStartElement("entry");
				xmlTextWriter.WriteElementString("id", System.Guid.NewGuid().ToString());
				xmlTextWriter.WriteElementString("message", "Addition");
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteString(NewLineCharacters);

				xmlTextWriter.Close();
			}

			//Read file content
			return File.ReadAllText(Constants.LogFilePath);
		}

		/// <summary>
		/// Chaining XmlReader and XmlWriter to process data.
		/// </summary>
		/// <returns></returns>
		public string UpdateByWriter()
		{
			var helper = new Helper();
			var ordersStream = helper.GetOrdersStream();
			var resultStream = new MemoryStream();

			var writerSettings = new XmlWriterSettings
			{
				ConformanceLevel = ConformanceLevel.Fragment
			};

			var readerSettings = new XmlReaderSettings
			{
				ConformanceLevel = ConformanceLevel.Fragment,
				ValidationType = ValidationType.None,
				ValidationFlags = XmlSchemaValidationFlags.None,
			};

			var reader = XmlReader.Create(ordersStream, readerSettings);
			var writer = XmlWriter.Create(resultStream, writerSettings);

			// Simply copies content but logic can be extended.
			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
				{
					writer.WriteStartElement(reader.Prefix, reader.LocalName, reader.NamespaceURI);
				}
				if (reader.NodeType == XmlNodeType.Text)
				{
					writer.WriteString(reader.Value);
				}
				if (reader.NodeType == XmlNodeType.EndElement)
				{
					writer.WriteEndElement();
				}
			}

			reader.Close();
			writer.Flush();

			resultStream.Position = 0;
			var streamReader = new StreamReader(resultStream);
			return streamReader.ReadToEnd();
		}

		#endregion

		#region Internal Implementations

		/// <summary>
		/// Writes sample orders content to writer instance.
		/// </summary>
		/// <param name="writer">Destination writer instance.</param>
		private void FillData(XmlWriter writer)
		{
			writer.WriteStartDocument();
			writer.WriteStartElement("Orders", Constants.OrdersNamespace);
			writer.WriteAttributeString("xmlns", "ns1", null, Constants.PricesNamespace);
			writer.WriteComment("Collection of orders");
				writer.WriteStartElement("Order", null);
				writer.WriteAttributeString("id", "1");
					writer.WriteElementString("Item", "Duff Beer");
					writer.WriteElementString("CustomerName", "Homer Simpson");
					writer.WriteElementString("Count", "4");
						writer.WriteStartElement("ns1", "TotalPrice", null);
						writer.WriteAttributeString("discount", "true");
							writer.WriteString("12.50");
						writer.WriteEndElement();
				writer.WriteEndElement();

				writer.WriteStartElement("Order", null);
				writer.WriteAttributeString("id", "2");
					writer.WriteElementString("Item", "Cherry Pie");
					writer.WriteElementString("CustomerName", "Marge Simpson");
					writer.WriteElementString("Count", "1");
						writer.WriteStartElement("ns1", "TotalPrice", null);
						writer.WriteAttributeString("discount", "false");
							writer.WriteString("5.15");
						writer.WriteEndElement();
				writer.WriteEndElement();
			writer.WriteEndElement();
			writer.WriteEndDocument();
			writer.Flush();
		}

		#endregion
	}
}