﻿using System;
using System.Text;
using System.Xml;
using Techtalk.XML.Resources;

namespace Techtalk.XML.Reader
{
	public class Processor
	{
		#region Private Fields

		private readonly Helper _helper;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes new instance of Techtalk.XML.Reader.Processor.
		/// </summary>
		public Processor()
		{
			_helper = new Helper();
		}

		#endregion

		#region Processing Methods

		/// <summary>
		/// Reads all content loaded by XmlReader instance
		/// </summary>
		/// <returns></returns>
		public string ReadReaderContent()
		{
			var result = new StringBuilder();
			var ordersStream = _helper.GetOrdersStream();
			var settings = new XmlReaderSettings();
			var reader = XmlReader.Create(ordersStream, settings);

			while (reader.Read())
			{
				result.Append(FormatElementStats(reader));


				if (reader.HasAttributes)
					while (reader.MoveToNextAttribute())
					{
						result.Append(FormatElementStats(reader));
					}
			}

			//release resources
			reader.Close();
			ordersStream.Close();

			return result.ToString();
		}

		/// <summary>
		/// Retrieves all values from CustomerName nodes
		/// </summary>
		/// <returns></returns>
		public string ReadExactNodes()
		{
			var result = new StringBuilder();
			var ordersStream = _helper.GetOrdersStream();
			var settings = new XmlReaderSettings
			{
				DtdProcessing = DtdProcessing.Ignore,
				IgnoreComments = true,
				IgnoreWhitespace = true,
				ConformanceLevel = ConformanceLevel.Document
			};

			var reader = XmlReader.Create(ordersStream, settings);

			while (reader.Read())
			{
				if (reader.LocalName == "CustomerName")
					result.AppendLine(reader.ReadElementContentAsString());
			}

			//release resources
			reader.Close();
			ordersStream.Close();

			return result.ToString();
		}

		/// <summary>
		/// Tries to create reader and validate against the schema.
		/// </summary>
		/// <returns>Reader content or validation result.</returns>
		public string ReadInvalidXml()
		{
			var ordersStream = _helper.GetOrdersEmptyStream();
			var settings = new XmlReaderSettings
			{
				ValidationType = ValidationType.Schema
			};
			settings.Schemas.Add(null, Constants.OrdersSchemaFilePath);
			settings.Schemas.Add(null, Constants.PricesSchemaFilePath);
			settings.ValidationEventHandler += (sender, e) =>
				Console.WriteLine($"{e.Severity} at {e.Exception.LineNumber}{e.Exception.LinePosition}. Message: {e.Message}");

			var reader = XmlReader.Create(ordersStream, settings);
			reader.MoveToContent();
			var result = reader.ReadInnerXml();

			reader.Close();
			ordersStream.Close();

			return result;
		}

		/// <summary>
		/// Retrieves data utilizing XmlNameTable object.
		/// </summary>
		/// <returns>Count of Author, Title nodes.</returns>
		public string ReadWithXmlNameTable()
		{
			var authorsCount = 0;
			var titlesCount = 0;

			var booksStream = _helper.GetBooksStream();

			var nt = new NameTable();
			object author = nt.Add("Author");
			object title = nt.Add("Title");

			var settings = new XmlReaderSettings
			{
				NameTable = nt
			};

			var reader = XmlReader.Create(booksStream, settings);

			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
				{

					// Cache the local name to prevent multiple calls to the LocalName property.
					object localname = reader.LocalName;

					// Do a comparison between the object references. This just compares pointers.
					if (ReferenceEquals(author, localname))
					{
						authorsCount++;
					}
					// Do a comparison between the object references. This just compares pointers.
					if (ReferenceEquals(title, localname))
					{
						titlesCount++;
					}
				}
			}

			reader.Close();
			booksStream.Close();

			return $"Authors: {authorsCount}; Titles: {titlesCount};";
		}

		#endregion

		#region Internal Implementations

		/// <summary>
		/// Retrieves element parameters.
		/// </summary>
		/// <param name="reader">Source reader.</param>
		/// <returns>Formatted element paramenters string.</returns>
		private static string FormatElementStats(XmlReader reader)
		{
			var result = new StringBuilder();

			result.Append($"Type: '{reader.NodeType}'\t\t");
			if (!string.IsNullOrEmpty(reader.LocalName))
				result.Append($"Name: '{reader.LocalName}'\t\t");

			if (!string.IsNullOrEmpty(reader.Value))
				result.Append($"Value: '{reader.Value}'\t\t");

			result.Append(Environment.NewLine);

			return result.ToString();
		}

		#endregion
	}
}