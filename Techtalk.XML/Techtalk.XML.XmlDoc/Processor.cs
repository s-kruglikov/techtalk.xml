﻿using System;
using System.Globalization;
using System.Text;
using System.Xml;
using Techtalk.XML.Resources;

namespace Techtalk.XML.XmlDoc
{
	public class Processor
	{
		#region Private Fields

		readonly Helper _helper;

		#endregion

		#region Constructor

		public Processor()
		{
			_helper = new Helper();
		}

		#endregion

		#region Public Interface

		/// <summary>
		/// Reads XmlDocument content.
		/// </summary>
		/// <returns>String representation of the xml nodes.</returns>
		public string ReadDocumentContent()
		{
			var resultBuilder = new StringBuilder();
			
			//XmlElement
			var rootElement = _helper.GetOrdersDocument().DocumentElement;

			//Iterate through the nodes and show stats
			ReadNode(rootElement, resultBuilder);

			return resultBuilder.ToString();
		}

		/// <summary>
		/// Creates sample XmlDocument instance
		/// </summary>
		/// <returns>String representation of the XmlDocument instance filled with Orders</returns>
		public string CreateDocumentContent()
		{
			var doc = new XmlDocument();

			//XmlElement
			var root = doc.CreateElement("Orders", Constants.OrdersNamespace);
			var order1 = doc.CreateElement("Order", Constants.OrdersNamespace);
			var item1 = doc.CreateElement("Item", Constants.OrdersNamespace);
			var customerName1 = doc.CreateElement("CustomerName", Constants.OrdersNamespace);
			var count1 = doc.CreateElement("Count", Constants.OrdersNamespace);
			var totalPrice1 = doc.CreateElement("ns1:TotalPrice", Constants.PricesNamespace);

			//XmlAttribute
			var discount1 = doc.CreateAttribute("discount");

			item1.InnerText = "Duff Beer";
			customerName1.InnerText = "Homer Simpson";
			count1.InnerText = "4";
			totalPrice1.InnerText = "12.50";
			discount1.InnerText = "true";

			doc.AppendChild(root);
			root.AppendChild(order1);
			order1.AppendChild(item1);
			order1.AppendChild(customerName1);
			order1.AppendChild(count1);
			
			totalPrice1.Attributes.Append(discount1);
			order1.AppendChild(totalPrice1);

			return doc.OuterXml;
		}

		/// <summary>
		/// Updates XmlDocument previosly filled with data.
		/// </summary>
		/// <returns></returns>
		public string UpdateDocumentContent()
		{
			XmlDocument doc = _helper.GetOrdersDocument();
			
			//Increase all totals to 10%;
			var totals = doc.GetElementsByTagName("TotalPrice", Constants.PricesNamespace);
			
			foreach (XmlElement total in totals) {
				try {
					var totalNum = decimal.Parse(total.InnerText);
					total.InnerText = (1.1M * totalNum).ToString(CultureInfo.InvariantCulture);
				} catch (Exception) {
					//Do nothing
				}
			}

			return doc.OuterXml;
		}
		
		/// <summary>
		/// Utilizes Xpath to retrieve elements.
		/// </summary>
		/// <returns>Sum of amounts in TotalPrice nodes.</returns>
		public string XpathUpdateDocumentContent()
		{
			XmlDocument doc = _helper.GetOrdersDocument();
			
			// If the XPath expression does not include a prefix, 
			// it is assumed that the namespace URI is the empty namespace. 
			// If your XML includes a default namespace,
			// you must still add a prefix and namespace URI to the XmlNamespaceManager;
			// otherwise, you will not get a node selected.
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
			nsmgr.AddNamespace("ns0", Constants.OrdersNamespace);
			nsmgr.AddNamespace("ns1", Constants.PricesNamespace);

			var totalSum = 0M;

			var orders = doc.SelectNodes("//ns0:Order", nsmgr);

			if (orders != null)
				foreach (XmlNode order in orders)
				{
					var orderAmountNode = order.SelectSingleNode("./ns1:TotalPrice", nsmgr);

					if (orderAmountNode != null)
						totalSum += decimal.Parse(orderAmountNode.InnerText);
				}

			return $"Total Amount: {totalSum}";
		}

		#endregion

		#region Internal Implementations

		/// <summary>
		/// Read single node and it's children.
		/// </summary>
		/// <param name="parentNode">Current node.</param>
		/// <param name="resultBuilder">Result StringBuilder instance with representation of node's data.</param>
		private void ReadNode(XmlNode parentNode, StringBuilder resultBuilder)
		{
			foreach (XmlNode node in parentNode) {
				resultBuilder.AppendLine(FormatNodeStats(node));

				if (node.HasChildNodes) {
					ReadNode(node, resultBuilder);
				}
			}
		}

		/// <summary>
		/// Formats node name, namespace, text and inner xml data into single string.
		/// </summary>
		/// <param name="node">Current node.</param>
		/// <returns>Node data string.</returns>
		private string FormatNodeStats(XmlNode node)
		{
			return $"Node Name: {node.Name}\r\n" +
					$"Node Namespace: {node.NamespaceURI}\r\n" +
					$"Node Text: {node.InnerText}\r\n" +
					$"Node Raw Xml: {node.InnerXml}\r\n";
		}

		#endregion
	}
}
