﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Techtalk.XML.Resources;

namespace Techtalk.XML.Xsd
{
	public class Processor
	{
		#region Private Fields

		private readonly Helper _helper;

		private const string DefaultSchemaNamespace = "http://www.w3.org/2001/XMLSchema";

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes new instance of the Xsd.Processor class.
		/// </summary>
		public Processor()
		{
			_helper = new Helper();
		}

		#endregion

		#region Public Interface

		/// <summary>
		/// Loads and validates xml schema from stream.
		/// </summary>
		/// <returns>String representation of loaded in memory schema.</returns>
		public string LoadValidateSchema()
		{
			try
			{
				var ordersSchema = XmlSchema.Read(_helper.GetOrdersSchemaStream(), ValidationCallback);

				//show output
				return SchemaToString(ordersSchema);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}

			return string.Empty;
		}

		/// <summary>
		/// Creates Orders schema.
		/// </summary>
		/// <returns>String representation of the created schema.</returns>
		public string CreateOrdersSchema()
		{
			// Create elements
			#region Elements

			var itemElement = new XmlSchemaElement
			{
				Name = "Item",
				SchemaTypeName = new XmlQualifiedName("string", DefaultSchemaNamespace)
			};

			var customerNameElement = new XmlSchemaElement
			{
				Name = "CustomerName",
				SchemaTypeName = new XmlQualifiedName("string", DefaultSchemaNamespace)
			};

			var countElement = new XmlSchemaElement
			{
				Name = "Count",
				SchemaTypeName = new XmlQualifiedName("unsignedByte", DefaultSchemaNamespace)
			};

			var totalPriceElement = new XmlSchemaElement
			{
				RefName = new XmlQualifiedName("TotalPrice", DefaultSchemaNamespace)
			};

			// id attribute.
			var idAttribute = new XmlSchemaAttribute
			{
				Name = "id",
				Use = XmlSchemaUse.Required,
				SchemaTypeName = new XmlQualifiedName("unsignedByte", DefaultSchemaNamespace)
			};

			//Order
			var orderElement = new XmlSchemaElement { Name = "Order" };
			var orderType = new XmlSchemaComplexType();

			var orderSequence = new XmlSchemaSequence
			{
				Items =
				{
					itemElement,
					customerNameElement,
					countElement,
					totalPriceElement
				}
			};
			orderType.Attributes.Add(idAttribute);
			orderType.Particle = orderSequence;
			orderElement.SchemaType = orderType;

			//Orders
			var ordersElement = new XmlSchemaElement { Name = "Orders" };
			var ordersType = new XmlSchemaComplexType();
			var ordersSequence = new XmlSchemaSequence
			{
				Items = { orderElement }
			};
			ordersType.Particle = ordersSequence;
			ordersElement.SchemaType = ordersType;

			#endregion

			// Create an empty schema.
			var ordersSchema = new XmlSchema
			{
				TargetNamespace = Constants.OrdersNamespace,
				Namespaces = new XmlSerializerNamespaces(new[] { new XmlQualifiedName("ns1", Constants.PricesNamespace) }),
			};

			var imports = new XmlSchemaImport
			{
				SchemaLocation = @".\Prices.xsd",
				Namespace = Constants.PricesNamespace
			};

			// Add all top-level element and types to the schema
			ordersSchema.Includes.Add(imports);
			ordersSchema.Items.Add(ordersElement);

			// Create an XmlSchemaSet to compile the customer schema.
			var schemaSet = new XmlSchemaSet();

			schemaSet.ValidationEventHandler += ValidationCallback;
			schemaSet.Add(ordersSchema);
			schemaSet.Compile();

			foreach (XmlSchema schema in schemaSet.Schemas())
			{
				ordersSchema = schema;
			}

			return SchemaToString(ordersSchema);
		}

		/// <summary>
		/// Traverse Orders schema.
		/// </summary>
		/// <returns></returns>
		public string TraverseSchema()
		{
			var resultBuilder = new StringBuilder();
			var ordersSchemaSet = GetOrdersCompiledSchemaSet();
			var ordersSchema = GetOrdersSchema(ordersSchemaSet);

			// Iterate over each XmlSchemaElement in the Values collection
			// of the Elements property.
			foreach (XmlSchemaElement element in ordersSchema.Elements.Values)
			{
				TraverseElements(element, resultBuilder);
			}

			return resultBuilder.ToString();
		}

		/// <summary>
		/// Adds elements to Orders schema.
		/// </summary>
		/// <returns>Updated Orders schema.</returns>
		public string UpdateSchema()
		{
			var ordersSchemaSet = GetOrdersCompiledSchemaSet();
			var ordersSchema = GetOrdersSchema(ordersSchemaSet);

			var totalElement = new XmlSchemaElement()
			{
				Name = "Total",
				SchemaTypeName = new XmlQualifiedName("unsignedByte", DefaultSchemaNamespace)
			};

			// Iterate over each XmlSchemaElement in the Values collection
			// of the Elements property.
			foreach (XmlSchemaElement element in ordersSchema.Elements.Values)
			{
				// If the qualified name of the element is "Customer", 
				// get the complex type of the Customer element  
				// and the sequence particle of the complex type.
				if (element.QualifiedName.Name.Equals("Orders"))
				{
					if (element.ElementSchemaType is XmlSchemaComplexType customerType)
					{
						// Add the new total element to the sequence.
						if (customerType.Particle is XmlSchemaSequence sequence) sequence.Items.Add(totalElement);
					}
				}
			}

			// Reprocess and compile the modified XmlSchema object.
			ordersSchemaSet.Reprocess(ordersSchema);
			ordersSchemaSet.Compile();

			var updatedOrdersSchema = GetOrdersSchema(ordersSchemaSet);
			
			return SchemaToString(updatedOrdersSchema);
		}

		#endregion

		#region Internal Implementations

		/// <summary>
		/// Simple callback method provides log to console.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		private static void ValidationCallback(object sender, ValidationEventArgs args)
		{
			switch (args.Severity)
			{
				case XmlSeverityType.Warning:
					Console.Write("WARNING: ");
					break;
				case XmlSeverityType.Error:
					Console.Write("ERROR: ");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			Console.WriteLine(args.Message);
		}

		/// <summary>
		/// Converts schema object to string.
		/// </summary>
		/// <param name="schema">String representation of the schema object.</param>
		/// <returns></returns>
		private static string SchemaToString(XmlSchema schema)
		{
			string result;

			using (var ms = new MemoryStream())
			{
				schema.Write(ms);

				ms.Seek(0, SeekOrigin.Begin);

				using (var reader = new StreamReader(ms))
				{
					result = reader.ReadToEnd();
				}
			}

			return result;
		}

		private static void TraverseElements(XmlSchemaElement element, StringBuilder resultBuilder)
		{
			resultBuilder.AppendLine($"Element: {element.QualifiedName}");

			if(element.ElementSchemaType is XmlSchemaComplexType complexType)
			{
				// Get the complex type of the element.

				// If the complex type has any attributes, get an enumerator 
				// and write each attribute name to the console.
				if (complexType.AttributeUses.Count > 0)
				{
					IDictionaryEnumerator enumerator =
						complexType.AttributeUses.GetEnumerator();

					while (enumerator.MoveNext())
					{
						var attribute = (XmlSchemaAttribute)enumerator.Value;

						resultBuilder.AppendLine($"Attribute: {attribute.QualifiedName}");
					}
				}

				if (complexType.ContentTypeParticle is XmlSchemaSequence sequence)
				{
					// Get the sequence particle of the complex type.

					// Iterate over each XmlSchemaElement in the Items collection.
					foreach (var item in sequence.Items)
					{
						var childElement = (XmlSchemaElement) item;

						TraverseElements(childElement, resultBuilder);
					}
				}
			}
		}

		/// <summary>
		/// Retrieves and complies Orders SchemaSet.
		/// </summary>
		/// <returns>Compiled schemaset containing Orders and Prices schemas.</returns>
		private XmlSchemaSet GetOrdersCompiledSchemaSet()
		{
			var ordersSchemaStream = _helper.GetOrdersSchemaStream();
			var pricesSchemaStream = _helper.GetPricesSchemaStream();

			var ordersReader = XmlReader.Create(ordersSchemaStream);
			var pricesReader = XmlReader.Create(pricesSchemaStream);

			//Add schema to SchemaSet and compile
			var schemaSet = new XmlSchemaSet();
			schemaSet.ValidationEventHandler += ValidationCallback;
			schemaSet.Add(null, ordersReader);
			schemaSet.Add(null, pricesReader);
			schemaSet.Compile();

			ordersReader.Close();
			pricesReader.Close();
			ordersSchemaStream.Close();
			pricesSchemaStream.Close();

			return schemaSet;
		}

		/// <summary>
		/// Retrieves Orders schema.
		/// </summary>
		/// <param name="schemaSet">Souce SchemaSet.</param>
		/// <returns>Orders schema object.</returns>
		private XmlSchema GetOrdersSchema(XmlSchemaSet schemaSet)
		{
			if (schemaSet == null) throw new ArgumentNullException(nameof(schemaSet));

			// Retrieve the compiled XmlSchema object from the XmlSchemaSet
			// by iterating over the Schemas property.
			XmlSchema schema = null;

			var schemaSetEnumerator = schemaSet.Schemas().GetEnumerator();
			if (schemaSetEnumerator.MoveNext())
			{
				schema = (XmlSchema)schemaSetEnumerator.Current;
			}

			return schema;
		}

		#endregion
	}
}
