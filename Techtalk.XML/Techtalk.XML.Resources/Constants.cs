﻿namespace Techtalk.XML.Resources
{
	/// <summary>
	/// Contains common constants
	/// </summary>
	public static class Constants
	{
		#region Paths

		private const string WORKSPACE_ROOT = @"..\..\..\Techtalk.XML.Resources\Samples\";

		public static string OrdersFilePath => $"{WORKSPACE_ROOT}Orders.xml";

		public static string OrdersEmptyFilePath => $"{WORKSPACE_ROOT}OrdersEmpty.xml";

		public static string OrdersSchemaFilePath => $"{WORKSPACE_ROOT}Orders.xsd";

		public static string OrdersTransformFilePath => $"{WORKSPACE_ROOT}Orders.xsl";

		public static string OrdersParamTransformFilePath => $"{WORKSPACE_ROOT}OrdersParam.xsl";

		public static string OrdersTransformedFilePath => $"{WORKSPACE_ROOT}OrdersTransformed.xml";

		public static string OrdersParamTransformedFilePath => $"{WORKSPACE_ROOT}OrdersParamTransformed.xml";

		public static string PricesSchemaFilePath => $"{WORKSPACE_ROOT}Prices.xsd";


		public static string BooksFilePath => $"{WORKSPACE_ROOT}Books.xml";

		public static string BooksSchemaFilePath => $"{WORKSPACE_ROOT}Books.xsd";


		public static string LogFilePath => $"{WORKSPACE_ROOT}LogSample.log";

		#endregion

		#region Namespaces

		public const string OrdersNamespace = @"http://schemas.orders.com";

		public const string PricesNamespace = @"http://schemas.prices.com";

		#endregion
	}
}