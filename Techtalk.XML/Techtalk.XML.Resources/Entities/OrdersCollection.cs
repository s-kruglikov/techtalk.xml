﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Techtalk.XML.Resources
{
	[Serializable]
	[XmlRoot(ElementName = "Orders", Namespace = Constants.OrdersNamespace)]
	public class OrdersCollection : List<Order>
	{
	}
}