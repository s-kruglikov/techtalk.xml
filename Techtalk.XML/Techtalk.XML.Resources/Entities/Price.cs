﻿using System;
using System.Xml.Serialization;

namespace Techtalk.XML.Resources
{
	[Serializable]
	public class Price
	{
		[XmlText]
		public decimal TotalPrice { get; set; }

		[XmlAttribute(attributeName: "discount", DataType = "boolean")]
		public bool Discount { get; set; }
	}
}