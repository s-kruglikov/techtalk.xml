<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                exclude-result-prefixes="msxsl">
    <xsl:output method="xml" indent="yes"/>
    <!--Input Param -->
    <xsl:param name="taxPercent"/>

    <xsl:template match="@* | node()">

        <Taxes xmlns="http://schemas.taxes.com">
            <xsl:for-each select="//*[local-name()='Order' and namespace-uri()='http://schemas.orders.com']">

                <xsl:variable name="totalPrice" select="./*[local-name()='TotalPrice']/text()"></xsl:variable>
                <xsl:variable name="taxCharge" select="$totalPrice * $taxPercent"/>

                <xsl:element name="Tax" namespace="http://schemas.taxes.com">
                    <xsl:element name="OrderId">
                        <xsl:value-of select="./@id"/>
                    </xsl:element>
                    <xsl:element name="CustomerName">
                        <xsl:value-of select="./*[local-name()='CustomerName']/text()"/>
                    </xsl:element>
                    <xsl:element name="TotalPrice">
                        <xsl:value-of select="$totalPrice"/>
                    </xsl:element>
                    <xsl:element name="TaxCharge">
                        <xsl:value-of select="$taxCharge"/>
                    </xsl:element>
                </xsl:element>
            </xsl:for-each>
            <xsl:element name="TotalTax">
                <xsl:value-of select="sum(//*[local-name()='TotalPrice']) * $taxPercent"/>
            </xsl:element>
            <xsl:element name="TaxPercent">
                <xsl:value-of select="$taxPercent * 100" />
            </xsl:element>
        </Taxes>
    </xsl:template>
</xsl:stylesheet>