<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:user="urn:my-scripts"
				exclude-result-prefixes="msxsl">
	<xsl:output method="xml" indent="yes"/>

	<msxsl:script language="C#" implements-prefix="user">
		<!--<msxsl:assembly name="system.assemblyName" />-->
		<!--<msxsl:assembly href="path-name" />-->
		<![CDATA[
		public int ReportDays(int daysCount){
			return daysCount + 4;
		}
		]]>
	</msxsl:script>

	<xsl:template match="@* | node()">
		<!--Variables-->
		<xsl:variable name="taxPercent" select="0.12"></xsl:variable>

		<Taxes xmlns="http://schemas.taxes.com">
			<xsl:for-each select="//*[local-name()='Order' and namespace-uri()='http://schemas.orders.com']">

				<xsl:variable name="totalPrice" select="./*[local-name()='TotalPrice']/text()"></xsl:variable>
				<xsl:variable name="taxCharge" select="$totalPrice * $taxPercent"/>

				<xsl:element name="Tax" namespace="http://schemas.taxes.com">
					<xsl:element name="OrderId">
						<xsl:value-of select="./@id"/>
					</xsl:element>
					<xsl:element name="CustomerName">
						<xsl:value-of select="./*[local-name()='CustomerName']/text()"/>
					</xsl:element>
					<xsl:element name="TotalPrice">
						<xsl:value-of select="$totalPrice"/>
					</xsl:element>
					<xsl:element name="TaxCharge">
						<xsl:value-of select="$taxCharge"/>
					</xsl:element>
				</xsl:element>
			</xsl:for-each>
			<xsl:element name="TotalTax">
				<xsl:value-of select="sum(//*[local-name()='TotalPrice']) * $taxPercent"/>
			</xsl:element>
			<xsl:element name="TaxDays">
				<xsl:value-of select="user:ReportDays(12)"/>
			</xsl:element>
		</Taxes>
	</xsl:template>
</xsl:stylesheet>