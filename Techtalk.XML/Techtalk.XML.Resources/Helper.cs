﻿using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace Techtalk.XML.Resources
{
	public class Helper
	{
		#region Public Interface
		
		public Stream GetOrdersStream()
		{
			return GetStream(Constants.OrdersFilePath);
		}

		public Stream GetOrdersEmptyStream()
		{
			return GetStream(Constants.OrdersEmptyFilePath);
		}

		public Stream GetOrdersSchemaStream()
		{
			return GetStream(Constants.OrdersSchemaFilePath);
		}

		public Stream GetOrdersTransformStream()
		{
			return GetStream(Constants.OrdersTransformFilePath);
		}

		public Stream GetOrdersParamTransformStream()
		{
			return GetStream(Constants.OrdersParamTransformFilePath);
		}

		public Stream GetPricesSchemaStream()
		{
			return GetStream(Constants.PricesSchemaFilePath);
		}

		public Stream GetBooksStream()
		{
			return GetStream(Constants.BooksFilePath);
		}

		/// <summary>
		/// Provides XmlDocument instance filled with Orders data.
		/// </summary>
		/// <returns>Orders data xml document.</returns>
		public XmlDocument GetOrdersDocument()
		{
			var xmlDoc = new XmlDocument();
			xmlDoc.Load(Constants.OrdersFilePath);

			return xmlDoc;
		}
		
		/// <summary>
		/// Returns XDocument instance filled with Orders data.
		/// </summary>
		/// <returns></returns>
		public XDocument GetOrdersXDocument()
		{
			return XDocument.Load(Constants.OrdersFilePath);
		}
		
		#endregion
			
		#region Internal Implementations
		
		/// <summary>
		/// Retrives Stream from file.
		/// </summary>
		/// <param name="filePath">File path.</param>
		/// <returns>File content as Stream.</returns>
		private Stream GetStream(string filePath)
		{
			if (!File.Exists(filePath))
				throw new FileNotFoundException("Unable to find file");
			
			var fileStream = File.Open(filePath, FileMode.Open);
			
			return fileStream;
		}
		
		#endregion
		
	}
}
