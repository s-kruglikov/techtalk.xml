﻿using System;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Techtalk.XML.Resources;

namespace Techtalk.XML.XPathDoc
{
	public class Processor
	{
		#region Private Fields

		private readonly Helper _helper;

		#endregion

		#region Constructors

		public Processor()
		{
			_helper = new Helper();
		}

		#endregion

		#region Public Interface

		/// <summary>
		/// Provides a fast, read-only, in-memory representation of an XML document using the XPath data model.
		/// </summary>
		/// <returns></returns>
		public string ReadDocumentContent()
		{
			var resultBuilder = new StringBuilder();

			using (var ordersStream = _helper.GetOrdersStream())
			{
				XPathDocument ordersDoc = new XPathDocument(ordersStream);

				XPathNavigator ordersDocNavigator = ordersDoc.CreateNavigator();

				XPathNodeIterator nodes =
					ordersDocNavigator.Select("/*[local-name()='Orders']/*[local-name()='Order']/*[local-name()='CustomerName']");

				while (nodes.MoveNext())
				{
					resultBuilder.Append(FormatElementStats(nodes.Current));
				}

				return resultBuilder.ToString();
			}
		}

		/// <summary>
		/// Evaluates an XPath expression
		/// </summary>
		/// <returns></returns>
		public string EvaluateXPathExpression()
		{
			using (var ordersStream = _helper.GetOrdersStream())
			{
				XPathDocument ordersDoc = new XPathDocument(ordersStream);

				XPathNavigator ordersDocNavigator = ordersDoc.CreateNavigator();

				XPathExpression query =
					ordersDocNavigator.Compile("sum(/*[local-name()='Orders']/*[local-name()='Order']/*[local-name()='TotalPrice']/text())");

				string total = ordersDocNavigator.Evaluate(query)?.ToString();

				return $"Total money amount: ${total}";
			}
		}

		/// <summary>
		/// Provides a fast, read-only, in-memory representation of an XML document using the XPath data model.
		/// </summary>
		/// <returns></returns>
		public string EvaluateWithNamespaces()
		{
			using (var ordersStream = _helper.GetOrdersStream())
			{
				XPathDocument ordersDoc = new XPathDocument(ordersStream);

				XPathNavigator ordersDocNavigator = ordersDoc.CreateNavigator();

				XmlNamespaceManager manager = new XmlNamespaceManager(ordersDocNavigator.NameTable);
				manager.AddNamespace("ns1", Constants.PricesNamespace);

				XPathExpression query = ordersDocNavigator.Compile("sum(//ns1:TotalPrice/text())");
				query.SetContext(manager);

				string total = ordersDocNavigator.Evaluate(query)?.ToString();

				return $"Total money amount: ${total}";
			}
		}

		/// <summary>
		/// Can edit an XML document created by the CreateNavigator method of the XmlDocument class
		/// </summary>
		/// <returns></returns>
		public string InsertNodes()
		{
			using (var ordersStream = _helper.GetOrdersStream())
			{
				XmlDocument ordersDoc = new XmlDocument();
				ordersDoc.Load(ordersStream);

				XPathNavigator ordersDocNavigator = ordersDoc.CreateNavigator();

				ordersDocNavigator.MoveToChild("Orders", Constants.OrdersNamespace);

				ordersDocNavigator.MoveToChild("Order", Constants.OrdersNamespace);

				do
				{
					ordersDocNavigator.MoveToChild("TotalPrice", Constants.PricesNamespace);

					ordersDocNavigator.InsertAfter("<IsBilled>true</IsBilled>");

					ordersDocNavigator.MoveToParent();
				} while (ordersDocNavigator.MoveToFollowing("Order", Constants.OrdersNamespace));

				ordersDocNavigator.MoveToRoot();

				return ordersDocNavigator.InnerXml;
			}
		}

		/// <summary>
		/// Can edit an XML document are created by the CreateNavigator method of the XmlDocument class.
		/// </summary>
		/// <returns></returns>
		public string EditNodes()
		{
			using (var ordersStream = _helper.GetOrdersStream())
			{
				XmlDocument ordersDoc = new XmlDocument();
				ordersDoc.Load(ordersStream);

				XPathNavigator ordersDocNavigator = ordersDoc.CreateNavigator();

				ordersDocNavigator.MoveToChild("Orders", Constants.OrdersNamespace);

				ordersDocNavigator.MoveToChild("Order", Constants.OrdersNamespace);

				do
				{
					ordersDocNavigator.MoveToChild("TotalPrice", Constants.PricesNamespace);
					ordersDocNavigator.MoveToFirstAttribute();

					ordersDocNavigator.SetValue("false");

					ordersDocNavigator.MoveToParent();
				} while (ordersDocNavigator.MoveToFollowing("Order", Constants.PricesNamespace));

				ordersDocNavigator.MoveToRoot();

				return ordersDocNavigator.InnerXml;
			}
		}

		#endregion

		#region Internal Implementations

		/// <summary>
		/// Retrieves element parameters.
		/// </summary>
		/// <param name="reader">Source reader.</param>
		/// <returns>Formatted element paramenters string.</returns>
		private static string FormatElementStats(XPathNavigator navigator)
		{
			var result = new StringBuilder();

			result.Append($"Type: '{navigator.NodeType}'\t\t");
			if (!string.IsNullOrEmpty(navigator.LocalName))
				result.Append($"Name: '{navigator.LocalName}'\t\t");

			if (!string.IsNullOrEmpty(navigator.Value))
				result.Append($"Value: '{navigator.Value}'\t\t");

			result.Append(Environment.NewLine);

			return result.ToString();
		}

		#endregion
	}
}