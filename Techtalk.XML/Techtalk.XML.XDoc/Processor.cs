﻿using System;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Techtalk.XML.Resources;

namespace Techtalk.XML.XDoc
{
	public class Processor
	{
		#region Constants

		private readonly XNamespace _defaultNamespace = Constants.OrdersNamespace;
		private readonly XNamespace _pricesNamespace = Constants.PricesNamespace;

		#endregion

		#region Private Fields

		private readonly Helper _helper;

		#endregion

		#region Constructor

		/// <summary>
		/// Initializes new instance of XDoc.Processor class.
		/// </summary>
		public Processor()
		{
			_helper = new Helper();
		}

		#endregion

		#region Public Interface

		/// <summary>
		/// Fills document with order data.
		/// </summary>
		/// <returns>String representation of the XDocument containing order data.</returns>
		public string CreateDocumentContent()
		{
			XDocument doc = new XDocument();

			XElement ordersElement = new XElement(_defaultNamespace + "Orders");
				ordersElement.Add(new XComment("Collection of orders"));

			XElement orderElement = new XElement(_defaultNamespace + "Order");
				orderElement.Add(new XAttribute("id", 1));
				orderElement.Add(new XElement(_defaultNamespace + "Item", "Duff Beer"));
				orderElement.Add(new XElement(_defaultNamespace + "CustomerName", "Homer Simpson"));
				orderElement.Add(new XElement(_defaultNamespace + "Count", "4"));

			XElement totalPriceElement = new XElement(_pricesNamespace + "TotalPrice", 12.50);
				totalPriceElement.Add(new XAttribute("discount", true));

			orderElement.Add(totalPriceElement);
			ordersElement.Add(orderElement);
			doc.Add(ordersElement);

			return doc.ToString();
		}

		/// <summary>
		/// Reads document content
		/// </summary>
		/// <returns></returns>
		public string ReadDocumentContent()
		{
			var result = new StringBuilder();
			result.AppendLine("Today we have in fridge:");

			var doc = _helper.GetOrdersXDocument();

			foreach (XElement order in doc.Element(_defaultNamespace + "Orders")
										.Elements(_defaultNamespace + "Order"))
			{
				result.AppendLine(string.Format("{0} for {1}",
												order.Element(_defaultNamespace + "Item").Value,
												order.Element(_defaultNamespace + "CustomerName").Value));
			}

			return result.ToString();
		}

		/// <summary>
		/// Converts XDocument instance into object representation.
		/// </summary>
		/// <returns>Object representation of the Orders.</returns>
		public string DocumentToObjects()
		{
			var resultBuilder = new StringBuilder();
			var doc = _helper.GetOrdersXDocument();
			var orderElements = doc.Element(_defaultNamespace + "Orders").Elements(_defaultNamespace + "Order");

			var orders = from xe in orderElements
						 where xe.Element(_defaultNamespace + "CustomerName").Value == "Homer Simpson"
						 select new Order
						 {
							Id = Convert.ToInt32(xe.Attribute("id").Value),
							Item = xe.Element(_defaultNamespace + "Item").Value,
							CustomerName = xe.Element(_defaultNamespace + "CustomerName").Value,
							Count = Convert.ToInt32(xe.Element(_defaultNamespace + "Count").Value),
							Price = new Price()
							{
								TotalPrice = Convert.ToDecimal(xe.Element(_pricesNamespace + "TotalPrice").Value),
								Discount = Convert.ToBoolean(xe.Element(_pricesNamespace + "TotalPrice").Attribute("discount").Value)
							}
						};

			foreach (var order in orders)
			{
				resultBuilder.Append(order.ToString());
			}

			return resultBuilder.ToString();
		}

		/// <summary>
		/// Adds new Order to document.
		/// </summary>
		/// <returns>Updated document content string.</returns>
		public string AddDocumentContent()
		{
			var doc = _helper.GetOrdersXDocument();
			var order = new Order
			{
				Id = 3,
				Item = "Chocolate bar",
				CustomerName = "Bart Simpson",
				Count = 1,
				Price = new Price
				{
					TotalPrice = 1.10M,
					Discount = false
				}
			};

			//Add new order to document
			doc.Descendants(_defaultNamespace + "Orders").First().Add(GetOrderElement(order));

			return doc.ToString();
		}

		/// <summary>
		/// Moves TotalPrice and Discount under Order.
		/// </summary>
		/// <returns>String representation of the updated document structure.</returns>
		public string UpdateDocumentContent()
		{
			var doc = _helper.GetOrdersXDocument();

			foreach (var order in doc.Element(_defaultNamespace + "Orders")
										.Elements(_defaultNamespace + "Order")
										.ToList())
			{
				var discount = order.Element(_pricesNamespace + "TotalPrice").Attribute("discount").Value;
				var totalPrice = order.Element(_pricesNamespace + "TotalPrice").Value;

				order.Element(_pricesNamespace + "TotalPrice").Remove();

				order.Add(new XElement(_defaultNamespace + "Discount", discount));
				order.Add(new XElement(_defaultNamespace + "TotalPrice", totalPrice));
			}

			return doc.ToString();
		}

		#endregion

		#region Internal implementations

		/// <summary>
		/// Converts order object to XElement.
		/// </summary>
		/// <param name="orderObj">Source order object.</param>
		/// <returns>XElement representation of the order.</returns>
		private XElement GetOrderElement(Order orderObj)
		{
			if (orderObj == null) throw new ArgumentNullException("orderObj");

			var orderElement = new XElement(_defaultNamespace + "Order");
			orderElement.Add(new XAttribute("id", orderObj.Id));
			orderElement.Add(new XElement(_defaultNamespace + "Item", orderObj.Item));
			orderElement.Add(new XElement(_defaultNamespace + "CustomerName", orderObj.CustomerName));
			orderElement.Add(new XElement(_defaultNamespace + "Count", orderObj.Count));

			var totalPriceElement = new XElement(_pricesNamespace + "TotalPrice", orderObj.Price.TotalPrice);
			totalPriceElement.Add(new XAttribute("discount", orderObj.Price.Discount));

			orderElement.Add(totalPriceElement);

			return orderElement;
		}

		#endregion
	}
}